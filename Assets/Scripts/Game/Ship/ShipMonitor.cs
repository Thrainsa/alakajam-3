﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ZBGE
{
  public class ShipMonitor : InteractOnButton
  {
    private GameObject activated;

    private bool activable = false;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
      activated = transform.Find("Activated").gameObject;
    }

    override protected void FixedUpdate()
    {
      base.FixedUpdate();

      Transform ship = transform.parent.parent;

      bool reallyActivable = activable && (
        !ship.GetComponent<ShipController>().HasPlayerControll() ||
        ship.GetComponent<Rigidbody2D>().velocity.magnitude < ShipController.SHIP_MAX_SPEED_TO_DETACH);
      if (reallyActivable != activated.activeSelf)
      {
        activated.SetActive(reallyActivable);
      }
    }

    public void ShipControlToggle(){
      var shipController = GetComponentInParent<ShipController>();
      if (!shipController.HasPlayerControll())
      {
        PlayerController.instance.TakeShipControl(shipController);
      }
      activable = true;
      activated.SetActive(true);
    }

    public void ShowInfo(){
      activable = true;
      activated.SetActive(true);
    }

    public void HideInfo(){
      activable = false;
      activated.SetActive(false);
    }

  }
}
