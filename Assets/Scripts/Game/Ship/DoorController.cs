﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE
{
  public class DoorController : MonoBehaviour {

    public bool opened;
    public bool docking;

    private ShipController dockingShip;

    public void OpenDoor(GameObject render) {
      opened = true;
      render.SetActive(false);
      GetComponent<AudioSource>().Play();
    }

    public void CloseDoor(GameObject render){
      opened = false;
      render.SetActive(true);
    }

    public void StartDocking(ShipController otherShip){
      dockingShip = otherShip;
      dockingShip.ToggleDockingMessage(true, GetModule().IsSwitchedOn() && otherShip.switchedOn);
      docking = true;
    }

    public void StopDocking()
    {
      if (dockingShip != null) dockingShip.ToggleDockingMessage(false, false);
      dockingShip = null;
      docking = false;
    }

    public ShipController getDockingShip(){
      return dockingShip;
    }

    public void ResetAfterDocking()
    {
      opened = false;
      StopDocking();
    }

    public ShipModule GetModule()
    {
      return transform.parent.parent.GetComponent<ShipModule>();
    }
  }
}
