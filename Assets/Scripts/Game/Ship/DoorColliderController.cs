﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE
{
  public class DoorColliderController : MonoBehaviour
  {
    
    private DoorController door;
    private GameObject doorRender;
    private ShipModule module;

    public void Awake()
    {
      door = transform.parent.GetComponent<DoorController>();
      doorRender = transform.parent.Find("Render").gameObject;
      module = door.GetModule();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
      int layer = other.gameObject.layer;
      if (module.IsSwitchedOn() && ((layer == 8 /* Character */ || layer == 10 /* Doors */)))
      {
        door.OpenDoor(doorRender);
        if (layer == 8)
        {
          ShipController ship = door.GetModule().GetDockingShip();
          if (ship != null)
          {
            ship.SwitchOn();
          }
        }
      }
    }

    void OnTriggerExit2D(Collider2D other)
    {
      int layer = other.gameObject.layer;
      if (layer == 8 /* Character */ || layer == 10 /* Doors */)
      {
        door.CloseDoor(doorRender);
      }
    }

  }

}