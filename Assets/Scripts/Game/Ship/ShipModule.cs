﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE
{
  public class ShipModule : MonoBehaviour {
    private DoorController[] doors = new DoorController[] { };

    [Header("References")]
    public Transform equipements;

    [Header("Prefabs")]
    public GameObject crewPrefab;
    public GameObject fuelPrefab;

    public int fuel;
    public int crewCount;

    private bool switchedOn = false;
    private float spawnMin = 0.4f;
    private float spawnMax = 0.7f;
    public Sprite[] crewMembersSprites;
    private List<GameObject> fuelObjects = new List<GameObject>();
    private List<GameObject> crewObjects = new List<GameObject>();

    void Awake()
    {
      doors = GetComponentsInChildren<DoorController>();
      SwitchOff();
    }

    public bool IsDocking(){
      foreach(var door in doors){
        if(door.docking){
          return true;
        }
      }
      return false;
    }

    public bool CanDock(){
      foreach(var door in doors){
        if(door.docking && !door.opened){
          return false;
        }
      }
      return true;
    }

    public ShipController GetShip()
    {
      return transform.parent.GetComponent<ShipController>();
    }

    public ShipController GetDockingShip(){
      foreach(var door in doors){
        if(door.getDockingShip() != null){
          return door.getDockingShip();
        }
      }
      return null;
    }

    public void ResetAfterDocking()
    {
      foreach(var door in doors){
        door.ResetAfterDocking();
      }
    }

    public void InitFuel(int fuel)
    {
      this.fuel = fuel;
      for (int i = 0; i < fuel; i++)
      {
        AddFuelPrefab();
      }
    }

    private void AddFuelPrefab()
    {
      var fuel = InstantiateEquipement(fuelPrefab);
      fuelObjects.Add(fuel);
    }

    public void InitCrewMembers(int crewMembers)
    {
      this.crewCount = crewMembers;
      for (int i = 0; i < crewMembers; i++)
      {
        AddCrewPrefab();
      }
    }

    private void AddCrewPrefab()
    {
      var crew = InstantiateEquipement(crewPrefab);
      var sprite = RandomPick(crewMembersSprites);
      crew.GetComponent<SpriteRenderer>().sprite = sprite;
      crewObjects.Add(crew);
    }

    private GameObject InstantiateEquipement(GameObject prefab)
    {
      GameObject equipment = GameManager.Instantiate(prefab, equipements);
      var posX = (UnityEngine.Random.Range(0,2) * 2 - 1) * UnityEngine.Random.Range(spawnMin, spawnMax);
      var posY = (UnityEngine.Random.Range(0,2) * 2 - 1) * UnityEngine.Random.Range(spawnMin, spawnMax);
      equipment.transform.localPosition = new Vector3(posX, posY, 0);
      return equipment;
    }

    private T RandomPick<T>(T[] values) {
      return values[UnityEngine.Random.Range(0, values.Length)];
    }

    public void RemoveFuel()
    {
      fuel--;
      GameObject.Destroy(fuelObjects[0].gameObject);
      fuelObjects.RemoveAt(0);
    }

    public void RemoveCrewMember()
    {
      crewCount--;
      GameObject.Destroy(crewObjects[0].gameObject);
      crewObjects.RemoveAt(0);
    }

    public void SwitchOff()
    {
      GetComponent<SpriteRenderer>().color = Color.gray;
      switchedOn = false;
    }

    public void SwitchOn()
    {
      GetComponent<SpriteRenderer>().color = Color.white;
      switchedOn = true;
    }

    public bool IsSwitchedOn()
    {
      return switchedOn;
    }

    internal void StopDocking()
    {
      foreach (var door in doors)
      {
        door.docking = false;
      }
    }
  }
}
