﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE {

  public class DockingModule : InteractOnTrigger {

    public DoorController doorController;

    public void StartDocking(){
      doorController.StartDocking(this.collider2D.GetComponentInParent<ShipController>());
    }

    public void StopDocking(){
      doorController.StopDocking();
    }
  }
}