﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System;

namespace ZBGE
{
  [RequireComponent(typeof(Rigidbody2D))]
  [RequireComponent(typeof(Collider2D))]
  public class ShipController : MonoBehaviour {

    // Components
    private PlayerInput playerInput;
    public new Rigidbody2D rigidbody2D;
    private AudioSource source;

    public float SHIP_CONTACT_TOLERANCE = 8;
    public const float SHIP_MAX_SPEED_TO_DETACH = 2;

    public GameObject explosionPrefab;

    public float maxForwardSpeed = 5f;
    public float acceleration = 1f;
    public float maxRotationSpeed = 60f;
    public float rotationAcceleration = 2f;
    public float invulnerabiltyTime = 1.5f;

    // Current properties
    private float currentForwardSpeed;
    private bool hasInputControll = false;

    public float maxO2 { get { return modules.Count * 1.5f ; }}
    public float maxFood { get { return modules.Count * 20f; }}
    public int maxFuel { get { return modules.Count * 2; }}
    public int maxCrewCount { get { return modules.Count * 2; }}

    private List<ShipModule> modules = new List<ShipModule>();

    private float lastDamageTime = 0f;

    // Current Equipement
    public bool switchedOn = false;
    public float o2;
    public float food;
    public int fuel;
    public int crewCount;
    private bool refillMode;
    private bool isHudFocus = false;

    public int Fuel {
      get { return fuel; }
    }

    public int CrewCount
    {
      get { return crewCount; }
    }

    public float O2
    {
      get { return o2; }
    }

    public bool IsInRefillMode
    {
      get { return refillMode; }
    }


    private readonly string[] SHIP_NAMES = new string[]
      {
        "Retribution",
        "The Colossus",
        "Crusher",
        "The Promise",
        "The Paladin",
        "B.S. Revolution",
        "STS Harmony",
        "B.C. Ravager",
        "B.C. Thunderstorm",
        "STS Invictus",
        "Thunderbird",
        "Andromeda",
        "Albatross",
        "Leviathan",
        "The Jellyfish",
        "SSE Gladiator",
        "C.S. Priestess",
        "USS Cyclopse",
        "B.C. Nostradamus",
        "SSE Falcon",
        "Tortoise",
        "Silent Watcher",
        "Liberator",
        "Torment",
        "Close Encounter",
        "ISS Dispatcher",
        "STS Endeavor",
        "USS Dark Phoenix",
        "C.S. Sandra",
        "HWSS Ares"
      };

    public string shipName = "";

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
      rigidbody2D = GetComponent<Rigidbody2D>();
      modules.AddRange(GetComponentsInChildren<ShipModule>());
      source = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
      playerInput = PlayerInput.instance;
      if (shipName == "")
      {
        shipName = SHIP_NAMES[UnityEngine.Random.Range(0, SHIP_NAMES.Length - 1)];
      }
    }

    protected bool IsMoveInput
    {
      get { return !Mathf.Approximately(playerInput.MoveInput.sqrMagnitude, 0f); }
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
      if (hasInputControll)
      {
        if (playerInput.ActionInput)
        {
          if (IsDocking() && CanDock())
          {
            bool dockingSuccess = Dock();
            if (!dockingSuccess)
            {
              PlayerController.instance.LeaveShip(this);
            }
          }
          else if (!IsDocking() && rigidbody2D.velocity.magnitude < SHIP_MAX_SPEED_TO_DETACH)
          {
            PlayerController.instance.LeaveShip(this);
          }
        }
        MoveShip();
      }

      if (isHudFocus)
      {
        HudController hud = HudController.instance;
        hud.SetShipName(shipName);
        hud.SetShipCrew(crewCount);
        hud.SetShipFuel(fuel);
        hud.SetShipOxygen(o2, maxO2, -ComputeO2DepletionPerSecond() * 60);
      }

      if (switchedOn)
      {
        o2 -= ComputeO2DepletionPerSecond() * Time.fixedDeltaTime;
        if (o2 < 0)
        {
          o2 = 0;
          KillCrewMember();
          if (hasInputControll)
          {
            InfoPanel.GetInstance(InfoPanel.PanelName.MESSAGE).SetColor(Color.red);
            InfoPanel.GetInstance(InfoPanel.PanelName.MESSAGE).ShowMessage("O2 SHORTAGE\nAll " + shipName + " crew deceased", 2000);
          }
        }
      }
    }

    /// <summary>
    /// This function will calculate the speed of the player
    /// </summary>
    void MoveShip() {
      // Cache the move input and cap it's magnitude at 1.
      Vector2 moveInput = playerInput.MoveInput;
      if (moveInput.sqrMagnitude > 1f) {
        moveInput.Normalize();
      }

      // Calculate the speed
      //var movement = new Vector3(moveInput.x, moveInput.y, 0) * maxForwardSpeed * Time.fixedDeltaTime;
      rigidbody2D.velocity += new Vector2(moveInput.x, moveInput.y) * acceleration;
      if (rigidbody2D.velocity.magnitude > maxForwardSpeed) {
        rigidbody2D.velocity = rigidbody2D.velocity.normalized * maxForwardSpeed;
      }
      source.volume = Mathf.Clamp(rigidbody2D.velocity.magnitude / maxForwardSpeed, 0, 1) * 0.7f;
      source.pitch = 1 + Mathf.Clamp(rigidbody2D.velocity.magnitude / maxForwardSpeed, 0, 1) / 0.4f;

      // Rotation
      rigidbody2D.angularVelocity -= playerInput.Rotation * rotationAcceleration;
      if (Math.Abs(rigidbody2D.angularVelocity) > maxRotationSpeed)
      {
        rigidbody2D.angularVelocity = rigidbody2D.angularVelocity / Mathf.Abs(rigidbody2D.angularVelocity) * maxRotationSpeed;
      }
    }

    internal List<ShipModule> GetModules()
    {
      return modules;
    }

    public void SwitchOn()
    {
      switchedOn = true;
      isHudFocus = true;
      foreach (var module in modules)
      {
        module.SwitchOn();
      }
    }

    public bool AreDockingShipsSwitchedOn()
    {
      if (IsDocking()) {
        bool on = true;
        foreach (var module in modules)
        {
          on = on && (module.GetDockingShip() == null || module.GetDockingShip().switchedOn);
        }
        return on;
      } else
      {
        return false;
      }
    }

    public bool IsDocking(){
      foreach(var module in modules){
        if(module.IsDocking()){
          return true;
        }
      }
      return false;
    }

    public bool CanDock(){
      bool allSwitchedOn = switchedOn && AreDockingShipsSwitchedOn();
      foreach(var module in modules){
        if(module.IsDocking() && !module.CanDock())
        {
          ToggleDockingMessage(false, false);
          return false;
        }
        allSwitchedOn = allSwitchedOn && module.IsSwitchedOn();
      }
      ToggleDockingMessage(true, allSwitchedOn);
      return true;
    }

    public void ToggleDockingMessage(bool enabled, bool allSwitchedOn)
    {
      if (enabled)
      {
        if (allSwitchedOn)
        {
          HudController.instance.ShowInfoMessage("DOCKING MODE ON");
        } else
        {
          HudController.instance.ShowInfoMessage("WARNING: SHIP POWERED OFF\nE.V.A. REQUIRED");
        }
      }
      else
      {
        HudController.instance.HideInfoMessage();
      }
    }

    public bool Dock()
    {
      ShipController dockingShip = null;
      foreach(var module in modules){
        if(module.IsDocking()){
          dockingShip = module.GetDockingShip();
        }
      }
      if (dockingShip == null || !dockingShip.switchedOn)
      {
        return false;
      }

      HudController.instance.ShowInfoMessage("DOCKING SUCCESSFUL", 0.5f, Color.white);

      foreach (var module in modules)
      {
        module.ResetAfterDocking();
      }

      var newModules = dockingShip.GetComponentsInChildren<ShipModule>();
      var localPos = modules[0].transform.localPosition;
      var delta = localPos - new Vector3(Mathf.RoundToInt(localPos.x), Mathf.RoundToInt(localPos.y), 0);
      
      if (newModules.Length >= modules.Count
        && (newModules.Length > modules.Count || dockingShip.shipName == "A.G. Hope"))
      {
        shipName = dockingShip.shipName;
      }

      var dockingShipGo = dockingShip.gameObject;
      Destroy(dockingShip);
      Destroy(dockingShipGo.GetComponent<CompositeCollider2D>());
      Destroy(dockingShipGo.GetComponent<Rigidbody2D>());
      dockingShipGo.SetActive(false);

      foreach(var module in newModules){
        module.transform.SetParent(this.transform);
        var pos = module.transform.localPosition - delta;
        module.transform.localPosition = new Vector3(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y), 0) + delta;
        module.ResetAfterDocking();
        modules.Add(module);
      }

      TakeShipGoods(dockingShip);

      // Needed to prevent wrong rotation if some module collision occured durring docking
      foreach(var module in modules){
        Vector3 rotation = module.transform.localRotation.eulerAngles;
        rotation = new Vector3(0,0,Mathf.RoundToInt(rotation.z / 90) * 90);
        module.transform.localRotation = Quaternion.Euler(rotation);
      }
      RecalculateCenter();

      return true;
    }

    public void TakeShipGoods(ShipController dockingShip)
    {
      this.fuel += dockingShip.fuel;
      this.o2 += dockingShip.o2;
      this.food += dockingShip.food;
      this.crewCount += dockingShip.crewCount;
      if (dockingShip.fuel != 0)
      {
        GameManager.instance.OnShipFuelChange();
      }
    }

    // Does not recalculate position. To use only for ship init
    public void AddModuleForInit(ShipModule shipModule)
    {
      this.modules.Add(shipModule);
      this.fuel += shipModule.fuel;
      this.crewCount += shipModule.crewCount;
      if (shipModule.fuel != 0)
      {
        GameManager.instance.OnShipFuelChange();
      }
    }

    public void RefillFood(float newFood){
      this.food = Mathf.Min(maxFood, food + newFood);
    }

    public void RefillFuel(int newFuel){
      this.fuel = Mathf.Min(maxFuel, fuel + newFuel);
      GameManager.instance.OnShipFuelChange();
    }

    public void SetRefillEnabled (bool enabled) {
      this.refillMode = enabled;
    }


  public bool AddCrewMember(int count){
      if(crewCount + count <= maxCrewCount){
        this.crewCount += count;
        return true; 
      } else {
        return false;
      }
    }

    public void RecalculateCenter(){
      Vector3 pivot = Vector3.zero;
      foreach(var module in modules){
        pivot += module.transform.localPosition;
      }
      pivot /= modules.Count;

      transform.position += pivot;
      foreach(var module in modules){
        module.transform.localPosition -= pivot;
      }
      if(this.hasInputControll){
        PlayerController.instance.transform.localPosition -= pivot;
        PlayerController.instance.anchoredPosition -= pivot;
      }
    }

    public void StartO2Supply()
    {
      refillMode = true;
    }

    public void StopO2Supply()
    {
      refillMode = false;
    }
    
    public void RemoveFuel(int fuelGoal)
    {
      fuel -= fuelGoal;
      int fuelRemoved = 0;
      for (int i = 0; i < maxFuel; i++)
      {
        foreach (var module in modules)
        {
          if(module.fuel > 0){
            fuelRemoved++;
            module.RemoveFuel();
            if(fuelRemoved >= fuelGoal){
              return;
            }
          }
        } 
      }
    }

    public void KillCrewMember()
    {
      if(this.crewCount > 0){
        this.crewCount = Mathf.Max(0, this.crewCount - 1);
        foreach (var module in modules)
        {
          if(module.crewCount > 0){
            module.RemoveCrewMember();
            return;
          }
        }
      }
    }

    public void ReleaseControl()
    {
      isHudFocus = false;
      hasInputControll = false;
      rigidbody2D.velocity = Vector2.zero;
      ToggleDockingMessage(false, false);
    }

    public void GainControl()
    {
      GameManager.instance.OnShipFuelChange();
      hasInputControll = true;
      isHudFocus = true;
      SwitchOn();
    }

    public bool HasPlayerControll(){
      return hasInputControll;
    }
    
    public float ComputeO2DepletionPerSecond()
    {
      var val = crewCount / 30f - (refillMode ? modules.Count * 0.20f : 0f);
      return o2 >= maxO2 && refillMode ? 0 : val;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
      if (collision.gameObject.GetComponent<PlanetController>() != null)
      {
        foreach (ContactPoint2D point in collision.contacts)
          StartCoroutine(TakeDamage(new Vector3(point.point.x, point.point.y)));
      }
      else if (collision.gameObject.GetComponent<ReplicantShipController>() != null
        || collision.gameObject.GetComponent<AsteroidController>() != null)
      {

        Destroy(collision.gameObject);
        foreach (ContactPoint2D point in collision.contacts)
        {
          StartCoroutine(TakeDamage(new Vector3(point.point.x, point.point.y)));
        }
      }
      else if (collision.gameObject.GetComponent<ShipController>() != null)
      {
        foreach (ContactPoint2D point in collision.contacts)
        {
          if (point.normalImpulse > SHIP_CONTACT_TOLERANCE)
          {
            Vector3 position = new Vector3(point.point.x, point.point.y);
            StartCoroutine(TakeDamage(position));
            StartCoroutine(collision.gameObject.GetComponent<ShipController>().TakeDamage(position));
          }
        }
      }
    }

    public IEnumerator TakeDamage(Vector3 position)
    {
      if(lastDamageTime + invulnerabiltyTime < Time.time){
        lastDamageTime = Time.time;
        if (this.crewCount == 0)
        {
          if (hasInputControll)
          {
            HudController.instance.ShowInfoMessage("GAME OVER\nShip damage: No crew left", 4, Color.red);
          }
          StartCoroutine(DestroyShip(hasInputControll));
        }
        else
        {
          if (hasInputControll)
          {
            HudController.instance.ShowInfoMessage("SHIP DAMAGE\n" + shipName + " lost a crew member", 3, Color.red);
          }
          KillCrewMember();
        }
      }
      GameObject explosion = Instantiate(explosionPrefab, position, Quaternion.identity, null);
      yield return new WaitForSeconds(1f);
      Destroy(explosion);
    }

    public IEnumerator DestroyShip(bool gameover)
    {
      if(this.hasInputControll){
        playerInput.ReleaseControl();
      }

      float totalTime = 2f;
      float timePerModule = totalTime / modules.Count;
      List<GameObject> explosions = new List<GameObject>();
      foreach (var item in modules)
      {
        item.StopDocking();
        explosions.Add(Instantiate(explosionPrefab, item.transform.position, Quaternion.identity, null));
        yield return new WaitForSeconds(UnityEngine.Random.Range(timePerModule*0.6f, timePerModule*1f));
      }
      foreach (var item in explosions)
      {
        Destroy(item);  
      }

      if(gameover)
      {
        float time = 0;
        while (time < 2f)
        {
          time += Time.deltaTime;
          transform.localScale *= 0.99f;
          yield return new WaitForEndOfFrame();
        }
        HudController.instance.FadeOut(HudController.instance.ExitToMenu);
      } else {
        Destroy(this.gameObject);
      }
    }
  }

}

