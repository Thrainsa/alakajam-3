﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE
{
  public class ReplicantShipController : MonoBehaviour
  {

    public AudioSource mergeSound;

    public float speed = 2;

    [Header("Internals")]
    public bool animatedDestruction = false;

    public Transform target = null;

    private Rigidbody2D r;

    void Start()
    {
      r = GetComponent<Rigidbody2D>();
      PlanetController planet = FindObjectOfType<PlanetController>();
      if (planet != null)
      {
        target = planet.transform;
        transform.right = target.position - transform.position; // Look at planet
      }
      HudController.instance.SetPlanetStatus(HudController.PlanetStatus.UNDER_ATTACK);
    }

    void Update()
    {
      if (animatedDestruction)
      {
        transform.localScale *= 0.98f;
        if (transform.localScale.x < 0.02)
        {
          Destroy(gameObject);
        }
      }
    }

    void FixedUpdate()
    {
      if (target)
      {
        r.velocity = speed * (target.position - transform.position).normalized;
      }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
      PlanetController planet = collision.gameObject.GetComponent<PlanetController>();
      if (!animatedDestruction && planet != null)
      {
        planet.Infect();
        animatedDestruction = true;
        mergeSound.Play();
        GetComponent<AudioSource>().Stop();
      }
    }

  }

}