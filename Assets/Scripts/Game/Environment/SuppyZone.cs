﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace ZBGE
{
  public class SuppyZone : InteractOnTrigger {

    public InfoPanel infoPanel;

    private List<ShipController> ships = new List<ShipController>();
    private bool panelHidden = true;

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    protected override void FixedUpdate()
    {
      var playerShipInside = HasPlayerShip();
      if(panelHidden && playerShipInside){
        infoPanel.ShowBlinkMessage("Refilling oxygen");
        panelHidden = false;
      } else if(!panelHidden && !playerShipInside){
        infoPanel.HideMessage();
        panelHidden = true;
      }
    }

    private bool HasPlayerShip()
    {
      foreach (var item in ships)
      {
        if(item.HasPlayerControll()){
          return true;
        }
      }
      return false;
    }

    protected override void ExecuteOnEnter(Collider2D other)
    {
      var ship = other.GetComponent<ShipController>();
      var replicant = other.GetComponent<ReplicantShipController>();
      if(ship != null){
        ship.StartO2Supply();
        ships.Add(ship);
      } else if(replicant){
        Destroy(this);
      }
      base.ExecuteOnEnter(other);
    }

    protected override void ExecuteOnExit(Collider2D other)
    {
      var ship = other.GetComponent<ShipController>();
      if(ship != null){
        ship.StopO2Supply();
        ships.Remove(ship);
      }
      base.ExecuteOnEnter(other);
    }

    /// <summary>
    /// This function is called when the MonoBehaviour will be destroyed.
    /// </summary>
    void OnDestroy()
    {
      GetComponentInChildren<SpriteRenderer>().color = Color.red;
      foreach (var text in GetComponentsInChildren<TextMeshProUGUI>())
      {
        text.color = Color.red;
        text.text = "Planet lost";
      }
      foreach (var ship in ships)
      {
        ship.StopO2Supply();
      }
    }
    
  }
}

