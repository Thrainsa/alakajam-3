﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goods : MonoBehaviour {

  public float minValue;
  public float maxValue;

  public float value;

  /// <summary>
  /// Start is called on the frame when a script is enabled just before
  /// any of the Update methods is called the first time.
  /// </summary>
  void Start()
  {
    value = Random.Range(minValue/2, maxValue/2) + Random.Range(minValue/2, maxValue/2);
  }

}
