﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrewMemberController : MonoBehaviour {

  private Transform module;

  private float nextMoveIn = 0;
  private Vector3? optTarget = null;

  private const float SPEED = 0.7f;
  private const float MOV_RANGE = 0.7f;

  void Start () {
    module = transform.parent.parent;
	}
	
	void FixedUpdate () {
    if (optTarget.HasValue)
    {
      // Move towards target
      Vector3 target = optTarget.Value;
      transform.localPosition += (target - transform.localPosition).normalized * SPEED * Time.fixedDeltaTime;
      if (Vector3.Distance(target, transform.localPosition) < .2f * SPEED) optTarget = null;
    }
    else if (nextMoveIn < 0)
    {
      // Wait a bit
      nextMoveIn = Random.Range(0f, 3f);
    }
    else
    {
      // Choose next target
      nextMoveIn -= Time.fixedDeltaTime;
      if (nextMoveIn < 0)
      {
        optTarget = new Vector3(Random.Range(-MOV_RANGE, MOV_RANGE), Random.Range(-MOV_RANGE, MOV_RANGE), 0);
      }
    }
		
	}
}
