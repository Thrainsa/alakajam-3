﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarfieldController : MonoBehaviour {

    public Transform optionalTarget = null;

    public float optionalTargetScale = 1;

    public List<MeshRenderer> images;

    public List<float> parallaxLevels;

    private Dictionary<MeshRenderer, float> imageMap = new Dictionary<MeshRenderer, float>();

    private Vector3 lastWorldPos;

	void Start () {
        int i = 0;
        foreach (MeshRenderer image in images)
        {
            if (i < parallaxLevels.Count) {
                imageMap[image] = parallaxLevels[i++];
            }
        }
        lastWorldPos = transform.position;
    }
	
	void Update () {
        Vector2 delta = transform.position - lastWorldPos;
        if(optionalTarget)
        {
            delta = optionalTargetScale * (optionalTarget.transform.position - lastWorldPos);
        }

        lastWorldPos = transform.position;
        if (delta.magnitude > 0)
        {
            foreach (KeyValuePair<MeshRenderer, float> pair in imageMap)
            {
                pair.Key.material.mainTextureOffset -= delta * pair.Value * 0.01f;
            }
        }
	}

    public void SetScale(float scale)
    {
        transform.localScale = new Vector3(1,1,1) * scale;
        if (images.Count > 0)
        {
            // hack: make last image transparent if zoomed out
            float alpha = Mathf.Max(0,1f - scale/2f);
            Color c = images[images.Count - 1].material.color;
            images[images.Count - 1].material.color = new Color(c.r, c.g, c.b, alpha);
        }
    }
}
