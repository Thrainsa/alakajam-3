﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE
{
  public class PlanetController : MonoBehaviour
  {

    public SpriteRenderer background;

    public SpriteRenderer foreground;

    public SpriteRenderer backgroundReplicant;

    public SpriteRenderer foregroundReplicant;

    [Header("Internals")]
    public bool infecting;

    public bool infected;

    public float showInfectionProgress = 0;

    public float TRANSITION_DELAY = 3f;

    public float EXPANSION_SPEED = 0.1f;

    private AudioSource replicantAmbient;

    void Start()
    {
      if (infected) showInfectionProgress = 1;

      background.gameObject.SetActive(true);
      foreground.gameObject.SetActive(true);
      backgroundReplicant.gameObject.SetActive(true);
      foregroundReplicant.gameObject.SetActive(true);

      replicantAmbient = GetComponent<AudioSource>();

      RefreshPlanet();
    }

    public void Infect()
    {
      infecting = true;
    }

    void FixedUpdate()
    {
      if (infecting && showInfectionProgress < 1)
      {
        showInfectionProgress += Time.fixedDeltaTime / TRANSITION_DELAY;
        if (showInfectionProgress >= 1)
        {
          infecting = false;
          infected = true;
          HudController.instance.SetPlanetStatus(HudController.PlanetStatus.LOST);
        }
        RefreshPlanet();
      }

      if (infected)
      {
        float expansion = EXPANSION_SPEED * Time.fixedDeltaTime;
        transform.localScale += new Vector3(expansion, expansion);
        float planetRadius = GetComponent<CircleCollider2D>().bounds.size.x / 2;
        replicantAmbient.minDistance = planetRadius * 0.8f;
        replicantAmbient.maxDistance = planetRadius * 1.2f;
      }

      Rotate(background, 1f * Time.fixedDeltaTime);
      Rotate(foreground, -0.5f * Time.fixedDeltaTime);
      Rotate(backgroundReplicant, 6f * Time.fixedDeltaTime);
      Rotate(foregroundReplicant, -4f * Time.fixedDeltaTime);
    }

    void RefreshPlanet()
    {
      SetAlpha(background, 1 - showInfectionProgress);
      SetAlpha(foreground, 1 - showInfectionProgress);
      SetAlpha(backgroundReplicant, showInfectionProgress);
      SetAlpha(foregroundReplicant, showInfectionProgress);
      replicantAmbient.volume = showInfectionProgress;
      if (!replicantAmbient.isPlaying)
      {
        replicantAmbient.Play();
      }
    }

    private void SetAlpha(SpriteRenderer r, float alpha)
    {
      if (r.color.a != alpha)
      {
        r.color = new Color(r.color.r, r.color.g, r.color.b, alpha);
      }
    }

    private void Rotate(SpriteRenderer r, float amount)
    {
      r.transform.Rotate(new Vector3(0, 0, amount));
    }
  }

}