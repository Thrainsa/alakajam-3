﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ZBGE
{
  [RequireComponent(typeof(Rigidbody2D))]
  [RequireComponent(typeof(Collider2D))]
  public class PlayerController : MonoBehaviour {
    private static PlayerController playerInstance;
    public static PlayerController instance { get { return playerInstance; } }

    // Components
    private PlayerInput playerInput;
    private new Rigidbody2D rigidbody2D;
    private SpriteRenderer sprite;
    private Animator animator;

    public float footMaxForwardSpeed = 1.5f;
    public float footAcceleration = 0.7f;
    public float footDrag = 20f;

    public float spaceMaxForwardSpeed = 3f;
    public float spaceAcceleration = 1f;
    public float spaceDrag = 1f;

    public Vector3 anchoredPosition = Vector3.zero;

    public AudioClip audioClipAirlock;
    public AudioClip audioClipWalk1;
    public AudioClip audioClipWalk2;
    public AudioClip audioClipAmbientShip;
    public AudioClip audioClipAmbientSpace;

    public AudioSource source;
    public AudioSource ambientSource;

    // Animator Const
    readonly int horizontalSpeed = Animator.StringToHash("horizontalSpeed");
    readonly int verticalSpeed = Animator.StringToHash("verticalSpeed");

    // Current properties
    private float currentForwardSpeed;
    private bool hasInputControll = true;
    public ShipController currentShip;
    private List<GameObject> shipFloorCollisions = new List<GameObject>();

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
      if (playerInstance == null)
        playerInstance = this;
      else if (playerInstance != this){
        // throw new UnityException("There cannot be more than one PlayerInstance script. The instances are " + playerInstance.name + " and " + name + ".");
        Destroy(this.gameObject);
      }

      playerInput = GetComponent<PlayerInput>();
      rigidbody2D = GetComponent<Rigidbody2D>();
      animator = GetComponent<Animator>();
      sprite = GetComponent<SpriteRenderer>();
      source = GetComponent<AudioSource>();
    }

    protected bool IsMoveInput
    {
      get { return !Mathf.Approximately(playerInput.MoveInput.sqrMagnitude, 0f); }
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
      if(hasInputControll){
        MovePlayer();
      } else
      {
        transform.localPosition = anchoredPosition;
      }
    }

    /// <summary>
    /// This function will calculate the speed of the player
    /// </summary>
    void MovePlayer() {
      // Cache the move input and cap it's magnitude at 1.
      Vector2 moveInput = playerInput.MoveInput;
      if (moveInput.sqrMagnitude > 1f) {
        moveInput.Normalize();
      }
      animator.SetFloat(horizontalSpeed, moveInput.x);
      animator.SetFloat(verticalSpeed, moveInput.y);

      // Calculate the speed
      bool inShip = shipFloorCollisions.Count > 0;
      float acceleration = inShip ? footAcceleration : spaceAcceleration;
      float maxForwardSpeed = inShip ? footMaxForwardSpeed : spaceMaxForwardSpeed;
      
        rigidbody2D.drag = inShip ? footDrag : spaceDrag;
        rigidbody2D.velocity += new Vector2(moveInput.x, moveInput.y) * acceleration;
        if (rigidbody2D.velocity.magnitude > maxForwardSpeed)
        {
          rigidbody2D.velocity = rigidbody2D.velocity.normalized * maxForwardSpeed;
        }

      if (rigidbody2D.velocity.magnitude > 1 && shipFloorCollisions.Count > 0)
      {
        if (!source.isPlaying)
        {
          source.clip = Random.Range(0f, 1f) < 0.5f ? audioClipWalk1 : audioClipWalk2;
          source.Play();
        }
      }
    }

    public void TakeShipControl(ShipController shipController){
      Debug.Log("TakeShipControl "+shipController.gameObject.name + "!");

      this.transform.SetParent(shipController.transform);

      currentShip = shipController;
      ReleaseControl();
      shipController.GainControl();

      StartCoroutine(PlayerInputWait(0.2f));

      CameraController.instance.target = shipController.transform;
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
      if (collider.gameObject.layer == 13 /*Floor*/)
      {
        if (shipFloorCollisions.Count == 0)
        {
          ambientSource.clip = audioClipAmbientShip;
          ambientSource.Play();
        }
        shipFloorCollisions.Add(collider.gameObject);
      }
    }

    public void OnTriggerExit2D(Collider2D collider)
    {
      if (collider.gameObject.layer == 13 /*Floor*/)
      {
        shipFloorCollisions.Remove(collider.gameObject);
        if (shipFloorCollisions.Count == 0)
        {
          source.clip = audioClipAirlock;
          source.Play();
          ambientSource.clip = audioClipAmbientSpace;
          ambientSource.Play();
        }
      }
    }

    public void LeaveShip(ShipController shipController){
      Debug.Log("LeaveShip "+shipController.gameObject.name + "!");
      this.transform.SetParent(GameObject.Find("----- Characters -----").transform);

      GainControl();
      shipController.ReleaseControl();

      StartCoroutine(PlayerInputWait(0.2f));

      CameraController.instance.target = transform;
    }

    IEnumerator PlayerInputWait(float waitTime){
      playerInput.ReleaseControl();
      yield return waitTime;
      playerInput.GainControl();
    }
    
    public void ReleaseControl()
    {
      this.hasInputControll = false;
      CameraController.instance.SetSize(
        CameraController.instance.GetSize() * 2);
      anchoredPosition = transform.localPosition;
    }

    public void GainControl()
    {
      this.hasInputControll = true;
      CameraController.instance.SetSize(
        CameraController.instance.GetSize() / 2);
      anchoredPosition = Vector3.zero;
    }
  }
}

