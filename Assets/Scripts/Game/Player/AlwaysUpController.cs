﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysUpController : MonoBehaviour {
  
	void LateUpdate () {
    transform.rotation = Quaternion.identity;
	}
}
