﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE
{
  public class CameraController : MonoBehaviour
  {

    private static CameraController cameraInstance;
    public static CameraController instance { get { return cameraInstance; } }

    public float TIGHTNESS = 0.2f;

    public float MIN_SIZE = 2;

    public float MAX_SIZE = 10;

    public Transform target;

    private Vector3 speed;

    private Camera c;

    private static float lastCameraSize;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
      if (cameraInstance == null) {
        cameraInstance = this;
        c = GetComponent<Camera>();
      }
      else if (cameraInstance != this)
        throw new UnityException("There cannot be more than one CameraController script. The instances are " + cameraInstance.name + " and " + name + ".");
    }

    void Start()
    {
      RefreshStarfield();
    }

    public void InitCameraPosition()
    {
      var heroShip = PlayerController.instance.GetComponentInParent<ShipController>();
      if (heroShip != null)
      {
        target = heroShip.transform;
        SetSize(lastCameraSize);
      }
      else
      {
        target = PlayerController.instance.transform;
      }
      c.transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);
    }

    void Update()
    {
      // Smooth follow target
      if (target != null)
      {
        Vector3 targetPosition = new Vector3(target.position.x, target.position.y, transform.position.z);
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref speed, TIGHTNESS);
      } else if (FindObjectOfType<ShipController>() && FindObjectOfType<MainTitleController>()) {
        target = FindObjectOfType<ShipController>().transform;
      }

      float wheel = -c.orthographicSize * Input.GetAxisRaw("Mouse ScrollWheel");
      if (wheel != 0)
      {
        SetSize(c.orthographicSize + wheel);
      }
    }

    public float GetSize()
    {
      return c.orthographicSize;
    }

    public void SetSize(float size)
    {
      float newSize = Mathf.Min(MAX_SIZE, Mathf.Max(MIN_SIZE, size));
      c.orthographicSize = newSize;
      lastCameraSize = newSize;
      RefreshStarfield();
    }

    void RefreshStarfield()
    {
      StarfieldController starfield = FindObjectOfType<StarfieldController>();
      if (starfield != null)
      {
        starfield.SetScale(c.orthographicSize / 5f);
      }
    }
  }
}