using UnityEngine;

namespace ZBGE
{

  public class PlayerInput : MonoBehaviour
  {

    private static PlayerInput inputInstance;
    public static PlayerInput instance { get { return inputInstance; } }

    private Vector2 movement = Vector2.zero;
    private float rotation = 0;
    private bool action = false;
    protected bool pause = false;
    private bool inputBlocked = false;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
      if (inputInstance == null)
        inputInstance = this;
      else if (inputInstance != this){
        // throw new UnityException("There cannot be more than one PlayerInput script. The instances are " + inputInstance.name + " and " + name + ".");
        Destroy(this.gameObject);
      }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
      if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)
                || Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
      {
        movement.Set(0, Input.GetAxis("Vertical"));
        rotation = Input.GetAxis("Horizontal");
      }
      else
      {
        movement.Set(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        rotation = Input.GetAxis("ShipRotation");
      }
      action = Input.GetButtonDown("Action");
      pause = Input.GetButtonDown("Pause");
    }

    public Vector2 MoveInput
    {
      get
      {
        if (inputBlocked)
          return Vector2.zero;
        return movement;
      }
    }

    public float Rotation
    {
      get { return rotation; }
    }

    public bool ActionInput
    {
      get { return action && !inputBlocked; }
    }

    public bool Pause
    {
      get { return pause; }
    }

    public void ReleaseControl()
    {
      inputBlocked = true;
    }

    public void GainControl()
    {
      inputBlocked = false;
    }
  }
}