﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace ZBGE
{
  public class HudController : MonoBehaviour {

    private static HudController hudInstance;
    public static HudController instance { get { return hudInstance; } }

    public enum PlanetStatus
    {
      NO_THREAT,
      COUNTDOWN,
      UNDER_ATTACK,
      LOST
    }

    public ControlsPanelController controlsGroup;

    public TextMeshProUGUI shipCrew;
    public TextMeshProUGUI shipFuel;
    public TextMeshProUGUI shipOxygen;

    public TextMeshProUGUI planetName;
    public TextMeshProUGUI planetLegend;
    public Image planetSprite;
    public GameObject planetGroupCountdown;
    public GameObject planetGroupUnderAttack;
    public GameObject planetGroupLost;
    public GameObject planetGroupNoThreat;
    public TextMeshProUGUI planetCountdownLabel;

    public PlanetStatus planetStatus;
    public AudioClip alarmClip;

    public Color redCountdown;

    public Image blackScreen;

    public TextMeshProUGUI shipName;

    private AudioSource audioSource;

    void Awake()
    {
      audioSource = GetComponent<AudioSource>();

      if (hudInstance == null)
        hudInstance = this;
      else if (hudInstance != this)
        throw new UnityException("There cannot be more than one HudInstance script. The instances are " + hudInstance.name + " and " + name + ".");

      FadeIn();
    }

    public void SetShipName(string newShipName)
    {
      shipName.text = newShipName;
    }

    public void FadeIn()
    {
      StartCoroutine(DoFadeIn());
    }

    private IEnumerator DoFadeIn()
    {
      blackScreen.gameObject.SetActive(true);
      blackScreen.color = Color.black;
      while (blackScreen.color.a > 0)
      {
        blackScreen.color = new Color(0, 0, 0, Mathf.Max(0, blackScreen.color.a - Time.deltaTime));
        yield return new WaitForEndOfFrame();
      }
      blackScreen.gameObject.SetActive(false);
    }

    public void FadeOut(System.Action callback)
    {
      StartCoroutine(DoFadeOut(callback));
    }

    private IEnumerator DoFadeOut(System.Action callback)
    {
      blackScreen.gameObject.SetActive(true);
      blackScreen.color = new Color(0, 0, 0, 0);
      while (blackScreen.color.a < 1)
      {
        blackScreen.color = new Color(0, 0, 0, Mathf.Min(1, blackScreen.color.a + Time.deltaTime));
        yield return new WaitForEndOfFrame();
      }
      if (callback != null)
      {
        callback.Invoke();
      }
    }


    public void SetShipCrew(int crewSize)
    {
      shipCrew.text = "x" + (crewSize < 10 ? "0" : "") + crewSize;
    }

    public void SetShipFuel(int fuelAmount)
    {
      shipFuel.text = "x" + (fuelAmount < 10 ? "0" : "") + fuelAmount;
    }

    public void SetShipOxygen(float oxygen, float max, float deltaPerMinute)
    {
      shipOxygen.text = oxygen.ToString("00.0") + "/" + max.ToString("00.0") + "L\n[";
      if (oxygen < max) {
        shipOxygen.text += Mathf.Abs(deltaPerMinute).ToString("00") + "/mn";
      } else
      {
        shipOxygen.text += "MAX";
      }
      shipOxygen.text += "]";
      shipOxygen.color = (deltaPerMinute <= 0) ? Color.white : Color.green;
      if (oxygen + deltaPerMinute / 60f * 15f < 0)
      {
        shipOxygen.color = Color.red;
      }
    }

    public void ShowInfoMessage(string message)
    {
      InfoPanel.GetInstance(InfoPanel.PanelName.MESSAGE).SetColor(Color.white);
      InfoPanel.GetInstance(InfoPanel.PanelName.MESSAGE).ShowMessage(message, 1000 + message.Length * 50f);
    }

    public void ShowInfoMessage(string message, float duration, Color color)
    {
      InfoPanel.GetInstance(InfoPanel.PanelName.MESSAGE).SetColor(color);
      InfoPanel.GetInstance(InfoPanel.PanelName.MESSAGE).ShowMessage(message, duration);
    }


    public void HideInfoMessage()
    {
      InfoPanel.GetInstance(InfoPanel.PanelName.MESSAGE).SetColor(Color.white);
      InfoPanel.GetInstance(InfoPanel.PanelName.MESSAGE).HideMessage();
    }

    public void ShowObjective(string message)
    {
      InfoPanel.GetInstance(InfoPanel.PanelName.OBJECTIVE).ShowMessage(message);
    }

    public void ShowControlsPanel()
    {
      controlsGroup.SetEnabled(true);
    }

    public void SetPlanetInfo(string name, int planetIndexStartOne, int planetCount, Sprite sprite)
    {
      string text = "Planet " + planetIndexStartOne + (MainTitleController.peacefulMode ? "" : ("/" + planetCount));
      planetName.text = name;
      planetLegend.text = text;
      planetSprite.sprite = sprite;
    }

    public void SetPlanetCountdown(float countdown)
    {
      countdown = Mathf.Max(0, countdown);
      string text = Mathf.Floor(countdown / 60).ToString("00") + ":" + Mathf.Floor(countdown % 60).ToString("00");
      planetCountdownLabel.text = text;
      planetCountdownLabel.color = (countdown < 5) ? redCountdown : Color.white;
    }

    public void SetPlanetStatus(PlanetStatus newStatus)
    {
      if (planetStatus != newStatus && newStatus != PlanetStatus.COUNTDOWN)
      {
        audioSource.clip = alarmClip;
        audioSource.Play();
      }
      planetStatus = newStatus;
      planetGroupNoThreat.SetActive(planetStatus == PlanetStatus.NO_THREAT);
      planetGroupCountdown.SetActive(planetStatus == PlanetStatus.COUNTDOWN);
      planetGroupUnderAttack.SetActive(planetStatus == PlanetStatus.UNDER_ATTACK);
      planetGroupLost.SetActive(planetStatus == PlanetStatus.LOST);
    }

    public void ExitToMenu()
    {
      MainTitleController.RegisterScore(0);
      GameManager.instance.Destroy();
      UnityEngine.SceneManagement.SceneManager.LoadScene("MainTitle");
    }
  }

}