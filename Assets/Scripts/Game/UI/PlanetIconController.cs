﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetIconController : MonoBehaviour
{

  private Image image;

  void Start()
  {
    image = GetComponent<Image>();
  }

  public void SetSprite(Sprite sprite)
  {
    image.sprite = sprite;
  }

  void Update()
  {
    transform.Rotate(0, 0, -0.15f);
  }
}
