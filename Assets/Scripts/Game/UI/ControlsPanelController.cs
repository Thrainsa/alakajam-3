﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE
{
  public class ControlsPanelController : MonoBehaviour
  {

    private new bool enabled = false;

    public void SetEnabled(bool enabled)
    {
      this.enabled = enabled;
      foreach (Transform child in transform)
      {
        child.gameObject.SetActive(enabled);
      }
    }

    void Update()
    {
      if (Input.GetKeyDown(KeyCode.Tab))
      {
        SetEnabled(!enabled);
      }
    }

  }

}