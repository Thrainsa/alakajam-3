﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ZBGE {
  public class InfoPanel : MonoBehaviour
  {
    public enum PanelName
    {
      MESSAGE,
      OBJECTIVE,
      EQUIPEMENT
    }

    private const float PERMANENT_MESSAGE = -1f;
    private const float TMP_MESSAGE = 3f;
    private static Dictionary<PanelName, InfoPanel> panelInstances = new Dictionary<PanelName, InfoPanel>();

    public PanelName panelName;
    public Vector3 hiddenPosition;
    public Vector3 visiblePosition;

    private RectTransform rectTransform;
    private TextMeshProUGUI textMeshProUGUI;

    [Header("Internals")]
    public Vector3 target;
    public float hideDelay = 0;
    public bool blinking;
    private float blinkingTime = 0f;
    private Vector3 movementSpeed = Vector3.zero;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
      panelInstances[panelName] = this;
      if (panelInstances[panelName] == null)
        panelInstances[panelName] = this;
      else if (panelInstances[panelName] != this)
        throw new UnityException("There cannot be more than one PanelInstance script with name " + panelName +". The instances are " + panelInstances[panelName].name + " and " + name + ".");

      rectTransform = GetComponent<RectTransform>();
      textMeshProUGUI = GetComponentInChildren<TextMeshProUGUI>();
    }

    void Start()
    {
      rectTransform.localPosition = target = hiddenPosition;
    }

    public static InfoPanel GetInstance(PanelName name)
    {
      return panelInstances[name];
    }

    public void ShowMessage(string message) {
      ShowMessage(message, PERMANENT_MESSAGE);
    }

    public void ShowTmpMessage(string message) {
      ShowMessage(message, TMP_MESSAGE);
    }

    public void ShowBlinkMessage(string message) {
      ShowMessage(message, PERMANENT_MESSAGE);
      blinking = true;
    }

    public void SetColor(Color color)
    {
      Image image = GetComponent<Image>();
      image.color = new Color(color.r, color.g, color.b, image.color.a);
      textMeshProUGUI.color = color;
    }

    public void ShowMessage(string message, float duration)
    {
      textMeshProUGUI.text = message;
      target = visiblePosition;
      hideDelay = duration;
    }

    public void HideMessage()
    {
      target = hiddenPosition;
    }

    public void Update()
    {
      if (hideDelay > 0)
      {
        hideDelay -= Time.fixedDeltaTime;
        if (hideDelay < 0)
        {
          target = hiddenPosition;
        }
      }

      if(blinking){
        blinkingTime += Time.deltaTime * 1f;
        textMeshProUGUI.alpha = Mathf.PingPong(blinkingTime, 1f);
      }
      
      rectTransform.localPosition = Vector3.SmoothDamp(rectTransform.localPosition, target, ref movementSpeed, 0.2f);
    }
    
  }
}