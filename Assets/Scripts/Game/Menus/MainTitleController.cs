﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace ZBGE
{
  public class MainTitleController : MonoBehaviour {

    private static int lastScore = 0;

    private static int highScore = 0;

    public static bool peacefulMode = false;

    public TextMeshProUGUI lastScoreValue;
    public TextMeshProUGUI highScoreValue;

    public bool destroyShip = true;
    
    public void Start()
    {
      LoadFromDisk();
      RegisterScore(Random.Range(0, 50));
      // Delete persistent objects
      if (destroyShip)
      {
        foreach (ShipController ship in FindObjectsOfType<ShipController>())
        {
          Destroy(ship.gameObject);
        }
      }
      foreach (GameManager gameManager in FindObjectsOfType<GameManager>())
      {
        Destroy(gameManager.gameObject);
      }

      lastScoreValue.text = lastScore + " citizens saved";
      highScoreValue.text = highScore + " citizens saved";
    }

    public void Update()
    {
      if (Input.GetKeyDown(KeyCode.Escape))
      {
        ExitGame();
      }
    }

    public static void RegisterScore(int score)
    {
      if (!peacefulMode)
      {
        lastScore = score;
        highScore = Mathf.Max(lastScore, highScore);
        SaveToDisk();
      }
    }

    public void StartGame(bool peaceful)
    {
      MainTitleController.peacefulMode = peaceful;
      SceneManager.LoadScene("StartLevel");
    }

    public void ExitGame()
    {
      Application.Quit();
    }

    public void BackToMenu()
    {
      SceneManager.LoadScene("MainTitle");
    }

    private static void LoadFromDisk()
    {
      try {
        var savePath = Application.persistentDataPath + "/saves.gd";
        if (File.Exists(savePath))
        {
          BinaryFormatter bf = new BinaryFormatter();
          FileStream file = File.Open(savePath, FileMode.Open);
          string json = (string)bf.Deserialize(file);
          SaveData saveData = (SaveData)JsonUtility.FromJson<SaveData>(json);

          highScore = saveData.highScore;

          file.Close();
        }
      } catch (System.Exception e) {
        Debug.LogError("Failed to load from disk: " + e.Message);
      }
    }

    private static void SaveToDisk()
    {
      var savePath = Application.persistentDataPath + "/saves.gd";

      SaveData saveData = new SaveData();
      saveData.highScore = highScore;

      BinaryFormatter bf = new BinaryFormatter();
      FileStream file = File.Create(savePath); 
      bf.Serialize(file, JsonUtility.ToJson(saveData));
      file.Close();
    }

  }
}
