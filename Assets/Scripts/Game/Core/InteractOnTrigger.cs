﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ZBGE
{
  [RequireComponent(typeof(Collider2D))]
  public class InteractOnTrigger : MonoBehaviour
  {
    public LayerMask layers;
    public UnityEvent OnEnter, OnExit;
    protected new Collider2D collider2D;

    protected virtual void FixedUpdate(){}

    /// <summary>
    /// Sent when another object enters a trigger collider attached to this
    /// object (2D physics only).
    /// </summary>
    /// <param name="other">The other Collider2D involved in this collision.</param>
    void OnTriggerEnter2D(Collider2D other)
    {
      // Debug.Log(other.gameObject.name + " enter on trigger of "+ gameObject.name);
      if (0 != (layers.value & 1 << other.gameObject.layer))
      {
        collider2D = other;
        ExecuteOnEnter(other);
      }
    }

    protected virtual void ExecuteOnEnter(Collider2D other)
    {
      OnEnter.Invoke();
    }

    /// <summary>
    /// Sent when another object leaves a trigger collider attached to
    /// this object (2D physics only).
    /// </summary>
    /// <param name="other">The other Collider2D involved in this collision.</param>
    void OnTriggerExit2D(Collider2D other)
    {
      // Debug.Log(other.gameObject.name + " exit on trigger of "+ gameObject.name);
      if (0 != (layers.value & 1 << other.gameObject.layer))
      {
        collider2D = null;
        ExecuteOnExit(other);
      }
    }

    protected virtual void ExecuteOnExit(Collider2D other)
    {
      OnExit.Invoke();
    }

    void OnDrawGizmos()
    {
      Gizmos.DrawIcon(transform.position, "InteractionTrigger", false);
    }
  } 
}
