﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ZBGE
{
  public class InteractOnButton : InteractOnTrigger
  {

    public string buttonName = "Action";
    public UnityEvent OnButtonPress;

    bool canExecuteButtons = false;

    protected override void ExecuteOnEnter(Collider2D other)
    {
      base.ExecuteOnEnter(other);
      canExecuteButtons = true;
    }

    protected override void ExecuteOnExit(Collider2D other)
    {
      base.ExecuteOnExit(other);
      canExecuteButtons = false;
    }

    void Update()
    {
      if (canExecuteButtons && Input.GetButtonDown(buttonName))
      {
        OnButtonPress.Invoke();
      }
    }
  } 
}
