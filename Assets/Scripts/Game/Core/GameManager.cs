﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ZBGE {
  public class GameManager : MonoBehaviour {

    private static GameManager managerInstance;
    public static GameManager instance { get { return managerInstance; } }

    [Header("References")]
    public Transform spawnerRoot;
    public PlanetController planete;

    [Header("Prefabs")]
    public ShipController shipPrefab;
    public ShipModule shipModulePrefab;

    // Data
    [Header("Datas")]
    public LevelsData LevelsData;
    public int currentLevel = 5;
    [HideInInspector]
    public Level currentLevelData;
    public int initialShipSize = 1;

    // Spawners
    private List<Vector3> shipSpawners = new List<Vector3>();
    private Vector3 bottomReplicantSpawner = Vector3.zero;
    private Vector3 heroShipSpawnner;

    // Utilities
    private List<Vector3> neighbours = new List<Vector3> { new Vector3(-2, 0, 0), new Vector3(0, 2, 0), new Vector3(2, 0, 0), new Vector3(0, -2, 0) };
    private GameObject lastHeroShip;

    // Invasion state
    public float timeBeforeInvasion = 1;
    public bool invasionStarted = false;

    private readonly string[] PLANET_NAMES = new string[]
    {
      "Moskucaro",
      "Osworia",
      "Zalomi",
      "Hoclars",
      "Toter",
      "Yuthea",
      "Strububos",
      "Shuyuvis",
      "Chilles 5Z65",
      "Cheshan D7L0",
      "Hasmeigawa",
      "Vaspiria",
      "Wubreth",
      "Iaclion",
      "Waotuné",
      "Seatov",
      "Spoyephus",
      "Glasuzuno",
      "Glomia E8",
      "Stosie FRW",
      "Jupraenus",
      "Sestater",
      "Vachorix",
      "Publyke",
      "Ieonus",
      "Haophus",
      "Frugagawa",
      "Strederus",
      "Prypso BQ",
      "Smillon I96",
    };

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
      if (managerInstance == null) {
        managerInstance = this;
        InitSpawners();
        DontDestroyOnLoad(this.gameObject);
      }
      else if (managerInstance != this) {
        managerInstance.spawnerRoot = this.spawnerRoot;
        managerInstance.planete = this.planete;
        Destroy(gameObject);
      }
    }

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
    }

    public void NextLevel()
    {
      // Remove fuel
      var heroShip = PlayerController.instance.GetComponentInParent<ShipController>();
      if (!heroShip || heroShip.fuel < currentLevelData.fuelGoal) {
        return;
      }
      heroShip.RemoveFuel(currentLevelData.fuelGoal);
      heroShip.rigidbody2D.velocity = Vector2.zero;

      // Don't destroy current hero ship / remove previous hero ship if it has changed
      var heroShipGameObject = heroShip.gameObject;
      heroShipGameObject.transform.SetParent(null);
      DontDestroyOnLoad(heroShipGameObject);
      if (lastHeroShip != null && lastHeroShip != heroShipGameObject) {
        Destroy(lastHeroShip);
      }
      lastHeroShip = heroShipGameObject;

      currentLevel++;
      if (currentLevel <= LevelsData.levels.Length || MainTitleController.peacefulMode)
      {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
      }
      else
      {
        MainTitleController.RegisterScore(heroShip.crewCount);
        SceneManager.LoadScene("EndLevel");
      }
    }


    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
      if (Input.GetKeyDown(KeyCode.Escape))
      {
        MainTitleController.RegisterScore(0);
        SceneManager.LoadScene("MainTitle");
      }
    }

    void FixedUpdate() {
      if (!invasionStarted && timeBeforeInvasion > 0) {
        timeBeforeInvasion -= Time.fixedDeltaTime;
        if (HudController.instance != null)
        {
          HudController.instance.SetPlanetCountdown(timeBeforeInvasion);
        }

        if (timeBeforeInvasion < 0)
        {
          invasionStarted = true;
          StartReplicantInvasion();
        }
      }

    }

    void StartReplicantInvasion()
    {
      for (float i = 0; i < currentLevelData.replicantCount; i++)
      {
        float radius = bottomReplicantSpawner.magnitude;
        Vector3 localPosition = radius * new Vector3(
          Mathf.Sin(i / currentLevelData.replicantCount * 2 * Mathf.PI + Mathf.PI),
          Mathf.Cos(i / currentLevelData.replicantCount * 2 * Mathf.PI + Mathf.PI),
          0);

        GameObject newObject = Instantiate(LevelsData.replicantShipPrefab, localPosition, Quaternion.identity);
        newObject.SetActive(true);
      }
    }

    public void LoadLevel()
    {
      if (MainTitleController.peacefulMode)
      {
        int baseLevelIndex = (currentLevel - 1) % LevelsData.levels.Length;
        currentLevelData = LevelsData.levels[baseLevelIndex].Copy();
        currentLevelData.fuelGoal = currentLevel + 1;
        if (currentLevel > 1)
        {
          currentLevelData.name = PLANET_NAMES[Random.Range(0, PLANET_NAMES.Length - 1)];
        }
      }
      else
      {
        currentLevelData = LevelsData.levels[currentLevel - 1];
      }
      Debug.Log("LoadLevel " + currentLevel + ", Planete: " + currentLevelData.planetName);

      // Init Ships
      InitShips();
      ReplacePlayerShip();

      // Init planete
      planete.background.sprite = currentLevelData.planetBackground;
      planete.foreground.sprite = currentLevelData.planetForeground;

      timeBeforeInvasion = MainTitleController.peacefulMode ? -1 : currentLevelData.timeBeforeInvasion;
      invasionStarted = false;

      HudController hud = HudController.instance;
      hud.SetPlanetCountdown(timeBeforeInvasion);
      hud.SetPlanetInfo(currentLevelData.planetName, currentLevel, LevelsData.levels.Length, currentLevelData.planetBackground);
      hud.SetPlanetStatus(MainTitleController.peacefulMode ? HudController.PlanetStatus.NO_THREAT : HudController.PlanetStatus.COUNTDOWN);
      if (currentLevelData.showControlsHelp)
      {
        hud.ShowControlsPanel();
      }
      OnShipFuelChange();
    }

    private void InitSpawners()
    {
      var spawners = spawnerRoot.GetComponentsInChildren<Spawner>();
      shipSpawners.Clear();
      
      foreach (var spawner in spawners)
      {
        switch (spawner.type)
        {
          case SpawnerType.HERO_SHIP:
            heroShipSpawnner = spawner.transform.position;
            break;
          case SpawnerType.SHIP:
            shipSpawners.Add(spawner.transform.position);
            break;
          case SpawnerType.REPLICANT:
            bottomReplicantSpawner = spawner.transform.position;
            break;
        }
      }
    }

    private void InitShips(){
      List<Vector3> activeSpawners = new List<Vector3>(shipSpawners);
      int activeSpawnersCount = Random.Range(
        (int) currentLevelData.activeSpawnerRange.x,
        (int) currentLevelData.activeSpawnerRange.y + 1);
      for (int i = 0; i < activeSpawnersCount; i++)
      {
        Vector3 position = RandomPick(activeSpawners);
        int moduleCount = RandomModuleCount();
        CreateShip(position, moduleCount, false);
        activeSpawners.Remove(position);
        if (activeSpawners.Count == 0) break;
      }
    }

    public void OnShipFuelChange ()
    {
      int currentFuel = (PlayerController.instance && PlayerController.instance.currentShip) ? PlayerController.instance.currentShip.Fuel : -1;
      if (currentFuel >= currentLevelData.fuelGoal)
      {
        ExitZoneController.instance.SetExitZoneAllowed(true);
        HudController.instance.ShowObjective("Objective: Go North and leave this planet (will use " + currentLevelData.fuelGoal + " units of fuel).");
      } else
      {
        ExitZoneController.instance.SetExitZoneAllowed(false);
        HudController.instance.ShowObjective("Objective: Retrieve " + currentLevelData.fuelGoal + " units of fuel.");
      }
    }

    public ShipController CreateShip(Vector3 position, int moduleCount, bool forceRotationToZero)
    {
      ShipController ship = GameManager.Instantiate(shipPrefab, position, Quaternion.identity);
      ship.AddModuleForInit(GenerateRandomModule(ship, Vector3.zero));
      while(ship.GetModules().Count < moduleCount){
        NewModule(ship);
      }
      ship.transform.parent = GameObject.Find("----- Gameplay -----").transform;
      ship.RecalculateCenter();
      ship.transform.position = position;
      ship.transform.Rotate(0, 0, forceRotationToZero ? 0 : Random.Range(0, 360));
      if (ship.ComputeO2DepletionPerSecond() > 0)
      {
        ship.o2 = Mathf.Min(ship.maxO2, ship.ComputeO2DepletionPerSecond() * currentLevelData.timeBeforeInvasion);
      }
      return ship;
    }

    private void ReplacePlayerShip(){
      var heroShip = PlayerController.instance.GetComponentInParent<ShipController>();
      if(heroShip == null){
        heroShip = CreateShip(heroShipSpawnner, initialShipSize, true);
        heroShip.SwitchOn();
        heroShip.shipName = "A.G. Hope";
        PlayerController.instance.transform.position = heroShipSpawnner + new Vector3(0, -0.5f, 0);
      }
      heroShip.transform.position = heroShipSpawnner;
      CameraController.instance.InitCameraPosition();
      CameraController.instance.target = heroShip.transform;
    }

    private int RandomModuleCount()
    {
      var levelRange = currentLevelData.shipSizeRange;
      return UnityEngine.Random.Range((int)levelRange.x, (int)levelRange.y + 1) ;
    }

    private ShipModule NewModule(ShipController ship)
    {
      ShipModule module = RandomPick(ship.GetModules());
      Vector3 desiredPos = RandomNeighbours(module.transform.localPosition);
      if (!ContainsModuleAtPos(ship.GetModules(), desiredPos))
      {
        var newModule = GenerateRandomModule(ship, desiredPos);
        ship.AddModuleForInit(newModule);
        return newModule;
      }
      return null;
    }

    public ShipModule GenerateRandomModule(ShipController ship, Vector3 localPosition){
      ShipModule module = GameManager.Instantiate(shipModulePrefab, ship.transform);
      module.transform.localPosition = localPosition;

      // Init module Data
      switch (UnityEngine.Random.Range(0,2))
      {
        case 0:
          module.InitCrewMembers(UnityEngine.Random.Range(0,3));
          module.InitFuel(UnityEngine.Random.Range(0,3 - module.crewCount));
          break;
        case 1:
          module.InitFuel(UnityEngine.Random.Range(0,3));
          module.InitCrewMembers(UnityEngine.Random.Range(0,3 - module.fuel));
          break;
      }
      return module;
    }

    private T RandomPick<T>(List<T> values) {
      return values[UnityEngine.Random.Range(0, values.Count)];
    }

    private Vector3 RandomNeighbours(Vector3 vector3){
      return vector3 + RandomPick(neighbours);
    }

    private bool ContainsModuleAtPos(List<ShipModule> modules, Vector3 desiredPos)
    {
      foreach (var item in modules)
      {
        if(item.transform.localPosition.Equals(desiredPos)){
          return true;
        }
      }
      return false;
    }

    public void Destroy(){
      if(lastHeroShip != null){
        Destroy(lastHeroShip);
      }
      Destroy(this.gameObject);
    }
  }
}

