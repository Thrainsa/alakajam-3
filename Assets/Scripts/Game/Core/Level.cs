﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "Level", order = 1)]
public class Level : ScriptableObject
{
  public bool showControlsHelp;
  public int fuelGoal;
  public Sprite planetBackground;
  public Sprite planetForeground;
  public string planetName;
  public float timeBeforeInvasion;
  public int replicantCount;
  public Vector2 activeSpawnerRange;
  public Vector2 shipSizeRange;

  public Level Copy()
  {
    Level l = new Level();
    l.showControlsHelp = this.showControlsHelp;
    l.fuelGoal = this.fuelGoal;
    l.planetBackground = this.planetBackground;
    l.planetForeground = this.planetForeground;
    l.planetName = this.planetName;
    l.timeBeforeInvasion = this.timeBeforeInvasion;
    l.replicantCount = this.replicantCount;
    l.activeSpawnerRange = this.activeSpawnerRange;
    l.shipSizeRange = this.shipSizeRange;
    return l;
  }
}