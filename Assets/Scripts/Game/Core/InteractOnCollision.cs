﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ZBGE
{
  [RequireComponent(typeof(Collider2D))]
  public class InteractOnCollision2D : MonoBehaviour
  {
    public LayerMask layers;
    public UnityEvent OnCollision;

    /// <summary>
    /// Sent when an incoming collider makes contact with this object's
    /// collider (2D physics only).
    /// </summary>
    /// <param name="other">The Collision2D data associated with this collision.</param>
    void OnCollisionEnter2D(Collision2D other)
    {
      if (0 != (layers.value & 1 << other.transform.gameObject.layer))
      {
        OnCollision.Invoke();
      }
    }

    void OnDrawGizmos()
    {
      Gizmos.DrawIcon(transform.position, "InteractionTrigger", false);
    }

  } 
}
