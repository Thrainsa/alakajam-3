﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabelsAroundCircleController : MonoBehaviour {

  public GameObject topLabel;

  public int LABEL_COUNT = 8;

  public int startIndex = -1;
  public int endIndex = -1;

  void Start () {
    
    // Generate labels
    for (float i = 1; i < LABEL_COUNT; i++)
    {
      if (startIndex == -1 || i >= startIndex)
      {
        if (endIndex == -1 || i <= endIndex)
        {
          float radius = topLabel.transform.localPosition.magnitude;
          Vector3 localPosition = radius * new Vector3(
            Mathf.Sin(i / LABEL_COUNT * 2 * Mathf.PI),
            Mathf.Cos(i / LABEL_COUNT * 2 * Mathf.PI),
            0);

          float zRotation = -i / LABEL_COUNT * 360;
          if (zRotation > 90 || zRotation < -90)
          {
            zRotation += 180;
          }

          GameObject newObject = Instantiate(topLabel, localPosition, Quaternion.Euler(0, 0, zRotation), transform);
          newObject.SetActive(true);
          newObject.transform.Translate(0, 0, 2); // Put labels in the background
        }
      }
    }
	}
	
}
