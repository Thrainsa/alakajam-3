﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE
{
  public class ExitZoneController : MonoBehaviour {

    private static ExitZoneController exitZoneInstance;
    public static ExitZoneController instance { get { return exitZoneInstance; } }

    public GameObject forbiddenExitObject;

    void Awake()
    {
      if (exitZoneInstance == null)
        exitZoneInstance = this;
      else if (exitZoneInstance != this)
      {
        throw new UnityException("There cannot be more than one ExitZoneController script. The instances are " + exitZoneInstance.name + " and " + name + ".");
      }
    }

    public void SetExitZoneAllowed(bool allowed)
    {
      if (forbiddenExitObject != null)
      {
        forbiddenExitObject.SetActive(!allowed);
      }
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
      var ship = collider.gameObject.GetComponent<ShipController>();
      if (forbiddenExitObject != null && !forbiddenExitObject.activeSelf && ship != null && ship.HasPlayerControll())
      {
        HudController.instance.FadeOut(GameManager.instance.NextLevel);
      }
    }

  }

}