﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE{
  public class GameLoader : MonoBehaviour {

    // Use this for initialization
    void Start () {
      GameManager.instance.LoadLevel();
    }
    
  }
}

