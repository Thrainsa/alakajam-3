﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AllLevels", menuName = "Levels", order = 1)]
public class LevelsData : ScriptableObject
{
  public GameObject replicantShipPrefab;
  public Level[] levels;
}